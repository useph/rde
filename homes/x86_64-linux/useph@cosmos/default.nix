{ config, pkgs, ... }: {
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "useph";
  home.homeDirectory = "/home/useph";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.05"; # Please read the comment before changing.

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages =
    let
      dmenu-bluetooth = pkgs.stdenv.mkDerivation {
        pname = "dmenu-bluetooth";
        version = "0.1.0";
        src = pkgs.fetchFromGitHub {
          owner = "Layerex";
          repo = "dmenu-bluetooth";
          rev = "96e2e3e1dd7ea2d2ab0c20bf21746aba8d70cc46";
          sha256 = "0G2PXWq9/JsLHnbOIJWSWWqfnBgOxaA8N2VyCbTUGmI=";
        };

        installPhase = ''
          mkdir -p $out/bin
          cp dmenu-bluetooth $out/bin
        '';
      };
    in
    with pkgs; [
      papirus-icon-theme
      materia-theme
      lxappearance
      xdg-user-dirs
      libnotify
      trash-cli

      dmenu-bluetooth
      libreoffice
      imagemagick
    ];


  fonts.fontconfig = {
    enable = true;
  };


  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.

  init = {
    archetypes.workstation = {
      enable = true;
    };
  };




  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/useph/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    EDITOR = "nvim";
  };

  gtk = {
    enable = true;
    # iconTheme = {
    #      name = "Papirus-Dark";
    #   package = pkgs.papirus-icon-theme;
    # };
    #
    # theme = {
    #      name = "Materia-dark";
    #      package = pkgs.materia-theme;
    #    };
    gtk3 = {
      extraConfig = {
        gtk-application-prefer-dark-theme = true;
      };
      bookmarks = [
        "file://${config.home.homeDirectory}/Documents/university"
        "file://${config.home.homeDirectory}/Media/Podcasts"
      ];
    };
  };

  # qt = {
  #   enable = true;
  #   platformTheme = "gtk";
  #   style = {
  #     name = "adwaita-dark";
  #   };
  # };

  # dconf.settings = {
  #   "org/gnome/desktop/interface" = {
  #     color-scheme = "prefer-dark";
  #   };
  # };

  # Let Home Manager install and manage itself.
  # programs.home-manager.enable = true;
}
