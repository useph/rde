{ pkgs, lib, ... }:
with lib;
with lib.init;
{
  imports = [
    ./hardware.nix
  ];

  init = {
    archetypes = {
      workstation.enable = true;
    };
    services = {
      syncthing.enable = true;
      transmission.enable = true;
      jackett.enable = true;
      printing.enable = true;
      openssh.enable = true;
    };
  };

  environment.shells = with pkgs; [ zsh ];

  programs.dconf.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
