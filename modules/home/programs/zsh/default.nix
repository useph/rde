{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.programs.zsh;
in
{
  options.init.programs.zsh = with types; {
    enable = mkEnableOption "zsh";
  };
  config = mkIf cfg.enable {
    # xdg.configFile = {
    #   "zsh/rc.d" = {
    #     source = ./config/rc.d;
    #     recursive = true;
    #   };
    # };

    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      enableCompletion = true;
      history = {
        # The maximum number of events stored in the internal history list.
        size = 1000000;
        # The maximum number of history events to save in the history file.
        save = 50000;
        # The file to save the history in when an interactive shell exists.
        path = "${config.xdg.stateHome}/zsh/zsh_history";
        # Ignore duplicate commands
        ignoreDups = true;
        # Ignore commands that start with space
        ignoreSpace = true;
        # Share command history data
        share = true;
        # When HISTFILE size exceeds HISTSIZE delete duplicates first
        expireDuplicatesFirst = true;
        extended = true;
      };
      # hash -d <name>=<path> makes ~<name> a shortcut for <path>.
      # You can use this ~name anywhere you would specify a dir, not just with `cd`!
      dirHashes = {
        trash = "${config.xdg.dataHome}/Trash";
        proj = "${config.xdg.userDirs.extraConfig.XDG_PROJECTS_DIR}";
        projects = "$(xdg-user-dir PROJECTS)";
        lbin = "${config.home.homeDirectory}/.local/bin";
        uni = "${config.xdg.userDirs.extraConfig.XDG_UNIVERSITY_DIR}";
      };
      # syntaxHighlighting = {
      #   enable = true;
      #   package = pkgs.zsh-fast-syntax-highlighting;
      # };
      plugins = [
        {
          name = "fast-syntax-highlighting";
          file = "fast-syntax-highlighting.plugin.zsh";
          src = pkgs.fetchFromGitHub {
            owner = "zdharma-continuum";
            repo = "fast-syntax-highlighting";
            rev = "cf318e06a9b7c9f2219d78f41b46fa6e06011fd9";
            sha256 = "sha256-RVX9ZSzjBW3LpFs2W86lKI6vtcvDWP6EPxzeTcRZua4=";
          };
        }
      ];
      initExtraBeforeCompInit = "";
      initExtraFirst = "";
      # Extra commands that should be added to {file}`.zshrc`.
      initExtra = ''
        ${builtins.readFile ./config/zshrc/options.zsh}
        ${builtins.readFile ./config/zshrc/environment.zsh}
        ${builtins.readFile ./config/zshrc/prompt.zsh}
        ${builtins.readFile ./config/zshrc/keybinding.zsh}
        ${builtins.readFile ./config/zshrc/functions.zsh}
        ${builtins.readFile ./config/zshrc/completion.zsh}
        ${builtins.readFile ./config/zshrc/lscolors.zsh}
        ${builtins.readFile ./config/zshrc/surround.zsh}
        ${builtins.readFile ./config/zshrc/clipboard.zsh}
        ${builtins.readFile ./config/zshrc/cursor.zsh}
        ${builtins.readFile ./config/zshrc/text-objects.zsh}
        ${builtins.readFile ./config/zshrc/nnn.zsh}
      '';
      envExtra = builtins.readFile ./config/zshenv;
      profileExtra = builtins.readFile ./config/zprofile;
    };
  };
}
