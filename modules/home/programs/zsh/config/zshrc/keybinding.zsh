
autoload edit-command-line; zle -N edit-command-line
autoload -U edit-command-line && zle -N edit-command-line && bindkey -M vicmd "^v" edit-command-line

bindkey "^?" backward-delete-char
# K in normal mode run man for the currect command
bindkey -M vicmd "K" run-help

# NOTE: uneeded in emacs mode
# allow ctrl-p, ctrl-n for navigate history
bindkey -M viins '^P' up-history
bindkey -M viins '^N' down-history

# Edit the command line using the visual editor
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd '^e' edit-command-line
bindkey -M emacs '^[e' edit-command-line

# Make Ctrl+Backspace kill a word backward
bindkey '^H' backward-kill-word

# remap there redo command since fzf uses ^R
bindkey -M vicmd 'U' redo
