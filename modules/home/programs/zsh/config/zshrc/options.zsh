# Remove unnecessary blanks from each command line being added to the history list.
setopt HIST_REDUCE_BLANKS

# Allow comments even in interactive shells.
setopt INTERACTIVE_COMMENTS

# Make cd push the old directory onto the directory stack.
setopt AUTO_PUSHD
# Don't push multiple copies of the same directory onto the directory stack.
setopt PUSHD_IGNORE_DUPS
# Do not print the directory stack after pushd or popd.
setopt PUSHD_SILENT

# Enable parameter expansion, command substitution and arithmetic expansion
# to be performed in prompts
setopt PROMPT_SUBST
