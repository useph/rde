zstyle ':completion:*' menu select

# list with colors
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zmodload zsh/complist
_comp_options+=(globdots)              # Include hidden files.
