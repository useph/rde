# A better cd
function cd {
	if [ $# -eq 1 ] && [ -e "$1" ] && [ ! -d "$1" ]; then
		local dir="$(dirname "$1")"
		builtin cd $dir
		echo $dir
	else
		builtin cd "$@"
	fi
}

bd () {
  (($#<1)) && {
    print -- "usage: $0 <name-of-any-parent-directory>"
    print -- "       $0 <number-of-folders>"
    return 1
  } >&2
  # example:
  #   $PWD == /home/arash/abc ==> $num_folders_we_are_in == 3
  local num_folders_we_are_in=${#${(ps:/:)${PWD}}}
  local dest="./"

  # First try to find a folder with matching name (could potentially be a number)
  # Get parents (in reverse order)
  local parents
  local i
  for i in {$num_folders_we_are_in..2}
  do
    parents=($parents "$(echo $PWD | cut -d'/' -f$i)")
  done
  parents=($parents "/")
  # Build dest and 'cd' to it
  local parent
  foreach parent (${parents})
  do
    dest+="../"
    if [[ $1 == $parent ]]
    then
      cd $dest
      return 0
    fi
  done

  # If the user provided an integer, go up as many times as asked
  dest="./"
  if [[ "$1" = <-> ]]
  then
    if [[ $1 -gt $num_folders_we_are_in ]]
    then
      print -- "bd: Error: Can not go up $1 times (not enough parent directories)"
      return 1
    fi
    for i in {1..$1}
    do
      dest+="../"
    done
    cd $dest
    return 0
  fi

  # If the above methods fail
  print -- "bd: Error: No parent directory named '$1'"
  return 1
}

_bd () {
  # Get parents (in reverse order)
  local num_folders_we_are_in=${#${(ps:/:)${PWD}}}
  local i
  for i in {$num_folders_we_are_in..2}
  do
    reply=($reply "`echo $PWD | cut -d'/' -f$i`")
  done
  reply=($reply "/")
}
compctl -V directories -K _bd bd

# Similar to mkdir, but changes the current directory to the created one
function take() {
  mkdir -p $@ && cd ${@:$#}
}

function md {
	[ $# = 1 ] && mkdir -p -- "$1" && cd -- "$1"
}

compctl -V directories md

function fcd()
{
	local fzfopt
	[ -n "$1" ] && fzfopt="-q $1"
	cd "$(find ~ -type d | grep -v "\.git" | fzf --height=40% --reverse --preview="tree -L 1 {}" --bind="ctrl-w:toggle-preview" --preview-window=:hidden $fzfopt)"
}

function lf()
{
	local fzfopt
	[ -n "$1" ] && fzfopt="-q $1"
	#find ~ -type f | grep -v "\.git" | fzf --height=40% --reverse -m $fzfopt| xargs -r "${EDITOR:-vi}"
	find ~ -type f | grep -v "\.git" | fzf --height=40% --reverse -m $fzfopt | xargs -I '{}' -r open '{}'
}

function lf2()
{
	local fzfopt
	[ -n "$1" ] && fzfopt="-q $1"
	#find ~ -type f | grep -v "\.git" | fzf --height=40% --reverse -m $fzfopt| xargs -r "${EDITOR:-vi}"
	fd --hidden | fzf --height=40% --reverse -m $fzfopt| xargs -I '{}' -r open '{}'
}

# TODO: fix the 'up /' bug
# TODO: fix if two dircetory have the same prefex bug
function up() {
	[ -d "${PWD%"$1"*}/"$1"" ] && cd "${PWD%"$1"*}/"$1""
  case "${1}" in
    (*[!0-9]*)  : ;;
    ("")        cd .. || return ;;
    ([0-9]*)         cd "$(eval "printf -- '../'%.0s {1..$1}")" || return ;;
  esac
  pwd
}

_up () {
  # Get parents (in reverse order)
  local num_folders_we_are_in=${#${(ps:/:)${PWD}}}
  local i
  for i in {$num_folders_we_are_in..2}
  do
    reply=($reply "`echo $PWD | cut -d'/' -f$i`")
  done
  reply=($reply "/")
}

compctl -V directories -K _up up
