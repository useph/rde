# Setting up the prompt

# TODO: Make the prompt shrink when the terminal column size is reduced.
# you can use this command `sed -e 's/\x1b\[[0-9;]*m//g'` to remove the ANSI escape code
autoload -Uz vcs_info
precmd_functions+=( vcs_info )
PS1='%n%F{9}@%f%M:%B%F{12}%3~%f%b${vcs_info_msg_0_}%f%(1j.[%j].)%(?.$.%F{9}\$)%f%b '

zstyle ':vcs_info:*' enable git

zstyle ':vcs_info:*' stagedstr '%F{2}M%f'
zstyle ':vcs_info:*' unstagedstr '%F{1}M%f'
zstyle ':vcs_info:*' check-for-changes true

zstyle ':vcs_info:*' actionformats '%F{3}[%B%F{2}%b%%b%f:%F{1}%a%F{3}]%f%c%u%f'
# zstyle ':vcs_info:*' formats '%F{3}[%B%F{2}%b%%b%F{3}]%f%c%u%f'
zstyle ':vcs_info:*' formats '%F{3}[%F{2}%b%F{3}]%f%c%u%f'

zstyle ':vcs_info:git*+set-message:*' hooks git-untracked
+vi-git-untracked() {
	if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
		[[
			$(
				git ls-files --other --directory --no-empty-directory --exclude-standard |
				sed q |
				wc -l |
				tr -d ' '
			) == 1
		]]; then
			hook_com[unstaged]+='%F{1}??%f'
	fi
}
