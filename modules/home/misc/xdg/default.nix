{ lib, config, ... }:
with lib;
let
  cfg = config.init.misc.xdg;
in
{
  options.init.misc.xdg = with types; {
    enable = mkEnableOption "xdg";
  };
  config = mkIf cfg.enable {
    xdg =
      let
        homeDirectory = config.home.homeDirectory;
      in
      {
        enable = true;
        userDirs = {
          enable = true;
          desktop = "${homeDirectory}";
          documents = "${homeDirectory}/Documents";
          download = "${homeDirectory}/Downloads";
          music = "${homeDirectory}/Media/Music";
          pictures = "${homeDirectory}/Media/Pictures";
          videos = "${homeDirectory}/Videos";
          templates = "${homeDirectory}/Templates";
          publicShare = "${homeDirectory}/Documents/Public";
          extraConfig = {
            XDG_PODCASTS_DIR = "${homeDirectory}/Media/Podcasts";
            XDG_MOVIES_DIR = "${homeDirectory}/Media/Videos/movies";
            XDG_PROJECTS_DIR = "${homeDirectory}/Code/Projects";
            XDG_UNIVERSITY_DIR = "${homeDirectory}/Documents/university";
            XDG_BIN_HOME = "${homeDirectory}/.local/bin";
          };
        };

        mime.enable = true;
        mimeApps.enable = true;
      };
  };
}
