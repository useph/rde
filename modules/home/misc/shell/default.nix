{ lib, config, ... }:
with lib;
let
  cfg = config.init.misc.shell;
in
{
  options.init.misc.shell = with types; {
    enable = mkEnableOption "shell";
  };
  config = mkIf cfg.enable {
    xdg.configFile = {
      "shell" = {
        source = ./config;
        recursive = true;
      };
    };
    home.file = {
      ".profile".source = ./.profile;
    };
  };
}
