#!/bin/sh

export PATH="$HOME/.local/bin":"$PATH"

# Default programs:
export EDITOR="nvim"
export VISUAL="$EDITOR"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export PAGER="less"
export FILE_MANAGER="n"



export GROFF_NO_SGR=1                  # for konsole and gnome-terminal

export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"

export ANDROID_PREFS_ROOT="$XDG_DATA_HOME/android"
export GOPATH="$XDG_DATA_HOME/go"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GPODDER_HOME="$XDG_CONFIG_HOME/gPodder"
