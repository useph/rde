{ lib, config, ... }:
with lib;
let
  cfg = config.init.misc.x11;
in
{
  options.init.misc.x11 = with types; {
    enable = mkEnableOption "x11";
  };
  config = mkIf cfg.enable {
    home.file = {
      ".xinitrc".source = ./xinitrc;
    };

    xsession = {
      enable = true;
      scriptPath = ".config/x11/xsession";
      profilePath = ".config/x11/xprofile";
      profileExtra = builtins.readFile ./config/xprofile;
    };
  };
}
