{ config, lib, ... }:
with lib;
let
  cfg = config.init.suites.desktop;
in
{
  options.init.suites.desktop = with types; {
    enable = mkEnableOption "Whether or not to enable common desktop configuration.";
  };

  config = mkIf cfg.enable {
    init = {
      misc = {
        shell.enable = true;
        x11.enable = true;
        xdg.enable = true;
      };

      programs = {
        zsh.enable = true;
      };
    };
  };
}
