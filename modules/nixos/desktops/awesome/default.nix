{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.desktop.awesome;
in
{
  options.init.desktop.awesome = with types; {
    enable = mkEnableOption "Whether or not to enable awesome desktop environment.";
  };
  config = mkIf cfg.enable {
    services = {
      displayManager.defaultSession = "none+awesome";
      xserver = {
        enable = true;
        windowManager.awesome.enable = true;
        displayManager.startx.enable = true;
      };
    };

    programs.slock.enable = true;
    init.programs = {
      sxhkd.enable = true;
      dunst.enable = true;
    };

    environment.systemPackages = with pkgs; [
      flameshot
      unclutter-xfixes
      cbatticon
      volumeicon

      xwallpaper

      keepassxc
      qbittorrent
      anki
      (st.overrideAttrs (_: {
        src = fetchFromGitHub {
          owner = "usephe";
          repo = "st";
          rev = "011ec9391257cdc9b98022657b5e2feee6f62a57";
          sha256 = "vU4Xdg3nw5ENGpuCLxifuhFzRaxVy+E4Gzb/08qTrMk=";

        };
      }))
      dmenu
      # (dmenu.overrideAttrs (_: {
      #   src = fetchFromGitHub {
      #     owner = "usephe";
      #     repo = "dmenu";
      #     name = "dmenu";
      #     rev = "192dca22a64ed6527c4745c5941975a21235b7bf";
      #     sha256 = "sha256-1kUlsGOl1klcsG94dwyopLbA5WZlen9uf1x+4VDCLEM=";
      #   };
      # }))
    ];


    init.home.extraOptions = {
      xdg.configFile = {
        "awesome" = {
          source = ./config;
          recursive = true;
        };
      };
    };
  };
}
