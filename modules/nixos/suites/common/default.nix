{ config, lib, ... }:
with lib;
let
  cfg = config.init.suites.common;
in
{
  options.init.suites.common = with types; {
    enable = mkEnableOption "Whether or not to enable common configuration.";
  };

  config = mkIf cfg.enable {
    init = {
      nix.enable = true;
      hardware = {
        networking.enable = true;
        audio.enable = true;
      };
      system = {
        boot.enable = true;
        fonts.enable = true;
        locale.enable = true;
        time.enable = true;
        xkb.enable = true;
      };
      tools = {
        fzf.enable = true;
        git.enable = true;
        misc.enable = true;
        nnn.enable = true;
        wget.enable = true;
        less.enable = true;
      };
    };
  };
}

