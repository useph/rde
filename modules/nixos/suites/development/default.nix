{ config, lib, ... }:
with lib;
let
  cfg = config.init.suites.development;
in
{
  options.init.suites.development = with types; {
    enable = mkEnableOption "Whether or not to enable common development configuration.";
  };

  config = mkIf cfg.enable {
    init = {
      programs = {
        tmux.enable = true;
        neovim.enable = true;
        idea.enable = false;
      };
      virtualisation = {
        kvm.enable = true;
        docker.enable = true;
      };
    };
  };
}
