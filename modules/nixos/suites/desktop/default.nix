{ config, lib, ... }:
with lib;
let
  cfg = config.init.suites.desktop;
in
{
  options.init.suites.desktop = with types; {
    enable = mkEnableOption "Whether or not to enable common desktop configuration.";
  };

  config = mkIf cfg.enable {
    services.playerctld.enable = true;
    init = {
      desktop = {
        awesome.enable = true;
      };
      programs = {
        firefox.enable = true;
        mpv.enable = true;
        zathura.enable = true;
        nsxiv.enable = true;
        emacs.enable = true;
        newsboat.enable = true;
        pcmanfm.enable = true;
      };
    };
  };
}
