{ options, config, pkgs, lib, inputs, ... }:
with lib;
let
  cfg = config.init.home;
in
{
  options.init.home = with types; {
    file = mkOption {
      type = attrs;
      description = "A set of files to be managed by home-manager's `home.file`.";
    };
    configFile = mkOption {
      type = attrs;
      description = "A set of files to be managed by home-manager's `xdg.configFile`.";
    };
    extraOptions = mkOption {
      type = attrs;
      description = "Options to pass directly to home-manager.";
    };
  };

  config = {
    init.home.extraOptions = {
      home.stateVersion = config.system.stateVersion;
      home.file = mkAliasDefinitions options.init.home.file;
      xdg.enable = true;
      xdg.configFile = mkAliasDefinitions options.init.home.configFile;
    };

    home-manager = {
      useUserPackages = true;
      useGlobalPkgs = true;

      users.${config.init.user.name} =
        mkAliasDefinitions options.init.home.extraOptions;
    };
  };
}
