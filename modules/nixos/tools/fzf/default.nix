{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.tools.fzf;
  userHome = config.home-manager.users.${config.init.user.name};
in
{
  options.init.tools.fzf = with types; {
    enable = mkEnableOption "fzf";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      fzf
    ];
    init.home.extraOptions = {
      programs.fzf = {
        enable = true;
        enableZshIntegration = true;
        defaultOptions = [
          "--history=${userHome.xdg.dataHome}/fzf/history"
          "--reverse"
          "--bind ctrl-n:down,ctrl-p:up,ctrl-j:next-history,ctrl-k:previous-history"
        ];
        tmux.enableShellIntegration = true;
      };
    };
  };
}
