{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.tools.less;
  userHome = config.home-manager.users.${config.init.user.name};
in
{
  options.init.tools.less = with types; {
    enable = mkEnableOption "less";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      less
    ];
    init.home.extraOptions = {
      home.sessionVariables = {
        # Have less display colours (from: https://wiki.archlinux.org/index.php/Color_output_in_console#man)
        # LESS_TERMCAP_mb = "$(printf '\e[1;32m')"; # begin bold
        # LESS_TERMCAP_md = "$(printf '\e[1;34m')"; # begin blink
        # LESS_TERMCAP_so = "$(printf '')"; # begin reverse video
        # LESS_TERMCAP_us = "$(printf '\e[01;31m')"; # begin underline
        # LESS_TERMCAP_me = "$(printf '\e[0m')"; # reset bold/blink
        # LESS_TERMCAP_se = "$(printf '\e[0m')"; # reset reverse video
        # LESS_TERMCAP_ue = "$(printf '\e[0m')"; # reset underline

        LESS_TERMCAP_mb = "$(tput bold; tput setaf 2)";
        LESS_TERMCAP_md = "$(tput bold; tput setaf 4)";
        LESS_TERMCAP_so = "$(tput bold; tput setaf 15; tput setab 0)";
        LESS_TERMCAP_us = "$(tput bold; tput setaf 1)";
        LESS_TERMCAP_me = "$(tput sgr0)";
        LESS_TERMCAP_se = "$(tput sgr0)";
        LESS_TERMCAP_ue = "$(tput sgr0)";

        LESS_TERMCAP_mr = "$(tput rev)";
        LESS_TERMCAP_mh = "$(tput dim)";
        LESS_TERMCAP_ZN = "$(tput ssubm)";
        LESS_TERMCAP_ZV = "$(tput rsubm)";
        LESS_TERMCAP_ZO = "$(tput ssupm)";
        LESS_TERMCAP_ZW = "$(tput rsupm)";


        LESSKEY = "${userHome.xdg.configHome}/less/lesskey";
        # TODO: make this directory manually
        LESSHISTFILE = "${userHome.xdg.stateHome}/less/history";
        LESS = "--ignore-case --clear-screen --mouse --RAW-CONTROL-CHARS";
      };
    };
  };
}
