{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.tools.wget;
  userHome = config.home-manager.users.${config.init.user.name};
in
{
  options.init.tools.wget = with types; {
    enable = mkEnableOption "wget";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      wget
    ];
    init.home.extraOptions = {
      xdg.configFile = {
        "wget/wgetrc".source = ./wgetrc;
      };
      home.shellAliases = {
        wget = "wget --hsts-file=${userHome.xdg.dataHome}/wget-hsts";
      };
      home.sessionVariables = {
        WGETRC = "${userHome.xdg.configHome}/wget/wgetrc";
      };
    };
  };
}
