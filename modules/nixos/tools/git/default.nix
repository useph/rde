{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.tools.git;
in
{
  options.init.tools.git = with types; {
    enable = mkEnableOption "git";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      git
    ];
    init.home.extraOptions = {
      programs.git = {
        enable = true;
        userName = "Useph El";
        userEmail = "el@useph.me";
        aliases = {
          st = "status --short --branch";
          undo = "reset --soft HEAD~1";
          last = "!git --no-pager log -1 HEAD --stat";
          # FIX: when we executing an external command this alias will show it like 'git ALIAS'='git !external-command'
          # which is not correct we should show it like 'git ALIAS'='external-command'
          alias = "!git config --get-regexp '^alias\\.' | sed -E -e \"s/^alias\\.([a-zA-Z1-9]+) (.+)/'git \\1'='git \\2'/\"";
          ll = "log --all --oneline --graph";
          log1 = "log --oneline --graph";
          ls = "ls-files";
          co = "checkout";
        };
        extraConfig = {
          core = {
            pager = "less -SR";
          };
          init = {
            defaultBranch = "main";
          };
          pager = {
            branch = false;
            config = false;
          };
          diff = {
            tool = "nvimdiff2";
            "exif" = {
              textconv = "exiftool";
            };
          };
          difftool = {
            prompt = false;
            "nvimdiff2" = {
              cmd = "nvim -d \$LOCAL \$REMOTE";
            };
          };
          merge = {
            tool = "nvimdiff4";
          };
          mergetool = {
            "nvimdiff4" = {
              cmd = "nvim -d \$LOCAL \$BASE $REMOTE $MERGED -c '\$wincmd w' -c '\$wincmd J'";
              trustExitCode = true;
            };
          };
        };
      };
    };
  };
}  
