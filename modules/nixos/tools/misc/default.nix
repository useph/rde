{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.init.tools.misc;
in
{
  options.init.tools.misc = with types; {
    enable = mkEnableOption "Whether or not to enable common utilities.";
  };

  config = mkIf cfg.enable {
    documentation.dev.enable = true;
    environment.systemPackages = with pkgs; [
      comma
      direnv
      file
      adwaita-icon-theme
      gnumake
      htop
      jq
      ltrace
      man-pages
      man-pages-posix
      mlocate
      ncdu
      nnn
      pamixer
      pkg-config
      rclone
      strace
      tree
      unrar
      unzip
      wget
      xcape
      xclip
      yt-dlp
      zip
    ];
  };
}
