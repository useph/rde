{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.tools.nnn;
in
{
  options.init.tools.nnn = with types; {
    enable = mkEnableOption "nnn";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      nnn
    ];
    init.home.extraOptions = {
      home.file = {
        ".config/nnn/nnnrc".source = ./nnnrc;
      };

      home.sessionVariables =
        let
          BLK = "03";
          CHR = "03";
          DIR = "04";
          EXE = "02";
          REG = "00";
          HARDLINK = "00";
          SYMLINK = "06";
          MISSING = "00";
          ORPHAN = "01";
          FIFO = "03";
          SOCK = "05";
          OTHER = "00";
        in
        {
          NNN_ARCHIVE = "\\.(rar|zip|tar)$";
          NNN_COLORS = "#0c020b09;4216";
          NNN_FCOLORS = "${BLK}${CHR}${DIR}${EXE}${REG}${HARDLINK}${SYMLINK}${MISSING}${ORPHAN}${FIFO}${SOCK}${OTHER}";
          NNN_FIFO = "/tmp/nnn.fifo";
          # NNN_OPENER = "open";
          NNN_TRASH = 1;
        };

      home.shellAliases = {
        n2 = "nnn -gARedQ";
      };

      programs.nnn = {
        enable = true;
        bookmarks =
          let
            userHome = config.home-manager.users.${config.init.user.name};
            documents = userHome.xdg.userDirs.documents;
            downloads = userHome.xdg.userDirs.download;
            university = userHome.xdg.userDirs.extraConfig.XDG_UNIVERSITY_DIR;
            movies = userHome.xdg.userDirs.extraConfig.XDG_MOVIES_DIR;
            projects = userHome.xdg.userDirs.extraConfig.XDG_PROJECTS_DIR;
            bin = userHome.xdg.userDirs.extraConfig.XDG_BIN_HOME;
            configHome = userHome.xdg.configHome;
            dataHome = userHome.xdg.dataHome;
          in
          {
            d = "${documents}";
            D = "${downloads}";
            u = "${university}";
            m = "${movies}";
            p = "${projects}";
            s = "${projects}/thirdparty";
            C = "${configHome}";
            l = "${dataHome}";
            b = "${bin}";
          };
      };
    };
  };
}
