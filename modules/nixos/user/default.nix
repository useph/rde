{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.init.user;
in
{
  options.init.user = with types; {
    name = mkOption {
      type = str;
      default = "useph";
      example = "Alice Q. User";
      description = "The name to use for the user account.";
    };
    initialPassword = mkOption {
      type = str;
      default = cfg.name;
      description = "The initial password to use when the user is first created.";
    };
    extraGroups = mkOption {
      type = (listOf str);
      default = [ ];
      description = "Groups for the user to be assigned.";
    };
    extraOptions = mkOption {
      type = attrs;
      default = { };
      description = "Extra options passed to <option>users.users.<name></option>.";
    };
  };

  config = {
    programs.zsh = {
      enable = true;
      enableGlobalCompInit = false;
      promptInit = "";
    };

    users.users.${cfg.name} = {
      isNormalUser = true;
      inherit (cfg) name initialPassword;


      home = "/home/${cfg.name}";
      group = "users";

      shell = pkgs.zsh;

      extraGroups = [ "wheel" "lp" ] ++ cfg.extraGroups;
    } // cfg.extraOptions;

    init.home.extraOptions = {
      home.shellAliases = {
        # Verbosity and settings.
        mv = "mv -vi";
        rm = "rm -vI";
        cp = "cp -vi";
        ## Display Monday as the first day of the week;
        cal = "cal -m";
        # Colorize commands when possible.
        ls = "ls --color=auto --group-directories-first -F";
        "l." = "ls -d .*";
        ll = "ls --human-readable -l";
        la = "ls -a";
        grep = "grep --exclude-dir={.git} --color=always";
        diff = "diff --color=always";
        e = "$\{EDITOR:-vi\}";
        tree = "tree -C";
      };

    };
  };

}
