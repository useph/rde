{ config, lib, pkgs, ... }:

with lib;
let
  cfg = config.init.virtualisation.kvm;
in
{
  options.init.virtualisation.kvm = with types; {
    enable = mkEnableOption "Whether or not to enable KVM virtualisation.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      virt-manager
    ];


    virtualisation = {
      libvirtd = {
        enable = true;
        qemu = {
          swtpm.enable = true;
          ovmf.enable = true;
          ovmf.packages = [ pkgs.OVMFFull.fd ];
        };
      };
      spiceUSBRedirection.enable = true;
    };

    init.user = {
      extraGroups = [ "libvirtd" ];
    };
  };
}
