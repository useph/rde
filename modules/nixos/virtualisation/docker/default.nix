{ config, lib, pkgs, ... }:

with lib;
let cfg = config.init.virtualisation.docker;
in
{
  options.init.virtualisation.docker = with types; {
    enable = mkEnableOption "Whether or not to enable Docker.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ podman-compose ];

    init.user = {
      extraGroups = [ "docker" ];
    };

    virtualisation = {
      docker = {
        enable = true;
      };
    };
  };
}
