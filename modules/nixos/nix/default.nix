{ lib, config, ... }:
with lib;
let
  cfg = config.init.nix;
in
{
  options.init.nix = with types; {
    enable = mkEnableOption "Whether or not to configure nix";
  };

  config = mkIf cfg.enable {
    nix = {
      settings.experimental-features = [ "nix-command" "flakes" ];
    };
  };
}
