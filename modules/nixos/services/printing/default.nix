{ config, lib, ... }:
with lib;
let
  cfg = config.init.services.printing;
in
{
  options.init.services.printing = with types; {
    enable = mkEnableOption "Whether or not to configure printing support.";
  };

  config = mkIf cfg.enable {
    services.printing.enable = true;
  };
}
