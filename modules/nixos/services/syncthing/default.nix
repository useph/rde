{ lib, config, ... }:
with lib;
let
  cfg = config.init.services.syncthing;
in
{
  options.init.services.syncthing = with types; {
    enable = mkEnableOption "Whether or not to configure syncthing";
  };

  config = mkIf cfg.enable {
    services = {
      syncthing = {
        enable = true;
        user = "useph";
        # TODO: make the home directory's name dynamic
        # Default folder for new synced folders
        dataDir = "/home/useph/Documents";
        # Folder for Syncthing's settings and keys
        configDir = "/home/useph/.config/syncthing";
        overrideDevices = true;
        overrideFolders = true;
      };
    };
    networking.firewall.allowedTCPPorts = [ 8384 22000 ];
    networking.firewall.allowedUDPPorts = [ 22000 21027 ];
  };
}
