{ lib, config, ... }:
with lib;
let
  cfg = config.init.services.openssh;
in
{
  options.init.services.openssh = with types; {
    enable = mkEnableOption "Whether or not to configure openssh support.";
  };

  config = mkIf cfg.enable {
    services.openssh = {
      enable = true;
    };
  };
}
