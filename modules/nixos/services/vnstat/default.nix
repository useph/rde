{ lib, config, ... }:
with lib;
let
  cfg = config.init.services.vnstat;
in
{
  options.init.services.vnstat = with types; {
    enable = mkEnableOption "Whether or not to configure vnstat";
  };

  config = mkIf cfg.enable {
    services.vnstat = {
      enable = true;
    };
  };
}
