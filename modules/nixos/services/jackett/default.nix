{ lib, config, ... }:
with lib;
let
  cfg = config.init.services.jackett;
in
{
  options.init.services.jackett = with types; {
    enable = mkEnableOption "Whether or not to configure jackett";
  };

  config = mkIf cfg.enable {
    services.jackett = {
      enable = true;
    };
  };
}
