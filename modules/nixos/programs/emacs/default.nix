{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.programs.emacs;
in
{
  options.init.programs.emacs = with types; {
    enable = mkEnableOption "emacs";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      emacs
      ispell
    ];
    init.home.extraOptions = {
      xdg.configFile = {
        "emacs/init.el".source = ./init.el;
      };
    };
  };
}
