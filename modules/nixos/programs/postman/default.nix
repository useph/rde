{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.init.programs.postman;
in
{
  options.init.programs.postman = with types; {
    enable = mkEnableOption "postman";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      postman
    ];
  };
}
