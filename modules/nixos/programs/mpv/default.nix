{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.programs.mpv;
in
{
  options.init.programs.mpv = with types; {
    enable = mkEnableOption "mpv";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      mpv
    ];
    init.home.extraOptions = {
      programs.mpv = {
        enable = true;
        config = {
          # Video format/quality that is directly passed to youtube-dl.
          # The aviablable formats can be found with the command ~youtube-dl --list-formats URL~
          ytdl-format = "bestvideo+bestaudio/best";

          # Select the english subtitles if available.
          slang = "en,eng";

          # Select the english audio track if available.
          alang = "en,eng";

          # Specify the sub font size
          sub-font-size = 30;

          # Load all subs containing the media filename
          sub-auto = "fuzzy";

          # Specify the OSD font size.
          osd-font-size = 20;

          sub-color = "#e4e4ef";
        };

        bindings = {
          # Unbind keys
          # f was taggling fullscreen
          f = "ignore";
          # double click with the mouse was taggling fullscreen
          MBTN_LEFT_DBL = "ignore";

          # Seek n seconds forward/backwards
          l = "seek 5";
          h = "seek -5";
          k = "seek 60";
          j = "seek -60";

          # Seek exactly n seconds forward or backward.
          # Don't show them on the OSD (no-osd).
          L = "no-osd seek  1 exact";
          H = "no-osd seek -1 exact";
          K = "no-osd seek  5 exact";
          J = "no-osd seek -5 exact";

          # Seek to the next/previous subtitle.
          # Don't show them on the OSD (no-osd).
          "Ctrl+l" = "no-osd sub-seek 1";
          "Ctrl+h" = "no-osd sub-seek -1";

          # Set position in current file to 0 seconds
          # Seek the beginning of the file
          "^" = "set time-pos 0";

          # Switch subtitle track
          n = "cycle sub";
          p = "cycle sub down";

          # Seek to the next/previous chapter
          N = "add chapter 1";
          P = "add chapter -1";

          # Skip to the next/previous file
          "Ctrl+n" = "playlist-next";
          "Ctrl+p" = "playlist-prev";

          # Toggle fullscreen
          F11 = "cycle fullscreen";

          # Increase/decrease the playback speed
          "[" = "add speed -0.5";
          "]" = "add speed 0.5";
          "{" = "add speed -0.1";
          "}" = "add speed 0.1";

          # Rotate the video clockwise, 90 degrees
          r = "cycle_values video-rotate 90 180 270 0";

          "Ctrl+f" = "script-binding quality_menu/video_formats_toggle";
          "Alt+f" = "script-binding quality_menu/audio_formats_toggle";
        };

        scripts = with pkgs.mpvScripts; [
          autoload
          quality-menu
          mpris
        ];

        scriptOpts = {
          osc = {
            # The deadzone is an area that makes the mouse act like leaving the window.
            # Movement there won't make the OSC show up and it will hide immediately if the
            # mouse enters it.
            # The deadzone starts at the window border opposite to the OSC and the size
            # controls how much of the window it will span. Values between 0.0
            # and 1.0, where 0 means the OSC will always popup with mouse movement in the
            # window, and 1 means the OSC will only show up when the mouse hovers it.
            deadzonesize = 0.75;
          };
        };
      };
    };
  };
}
