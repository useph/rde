{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.programs.newsboat;
in
{
  options.init.programs.newsboat = with types; {
    enable = mkEnableOption "newsboat";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      newsboat
    ];
    init.home.extraOptions = {
      xdg.configFile = {
        "newsboat" = {
          source = ./config;
          recursive = true;
        };
      };
      home.shellAliases = {
        feed = "newsboat -q";
      };
    };
  };
}
