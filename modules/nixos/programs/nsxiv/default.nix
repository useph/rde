{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.programs.nsxiv;
in
{
  options.init.programs.nsxiv = with types; {
    enable = mkEnableOption "nsxiv";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      (nsxiv.overrideAttrs (_: {
        patches = [ ./nsxiv-v32.patch ];
      }))
    ];
    init.home.extraOptions = {
      xdg.configFile = {
        "nsxiv" = {
          source = ./config;
          recursive = true;
        };
      };


      xresources.properties = {
        "Nsxiv.window.background" = "#0f0f0f";
        "Nsxiv.window.foreground" = "#9e9e9e";
        "Nsxiv.mark.foreground" = "#9e9e9e";
        "Nsxiv.bar.background" = "#181818";
        "Nsxiv.bar.foreground" = "#9e9e9e";
        "Nsxiv.bar.font" = "monospace:size=14:antialias:true:autohint=true:style=bold";
      };
      xdg.mimeApps = {
        defaultApplications = {
          "image/gif" = [ "nsxiv.desktop" ];
          "image/jpeg" = [ "nsxiv.desktop" ];
          "image/jpg" = [ "nsxiv.desktop" ];
          "image/png" = [ "nsxiv.desktop" ];
          "image/tiff" = [ "nsxiv.desktop" ];
          "image/webp" = [ "nsxiv.desktop" ];
        };
      };
    };
  };
}
