{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.init.programs.idea;
in
{
  options.init.programs.idea = with types; {
    enable = mkEnableOption "idea";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      jetbrains.idea-ultimate
    ];

    init.home.extraOptions = {
      xdg.configFile = {
        "ideavim/ideavimrc".source = ./ideavimrc;
      };
    };
  };
}
