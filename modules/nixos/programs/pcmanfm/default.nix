{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.init.programs.pcmanfm;
in
{
  options.init.programs.pcmanfm = with types; {
    enable = mkEnableOption "pcmanfm";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      pcmanfm
    ];
    init.home.extraOptions = {
      xdg.mimeApps = {
        defaultApplications = {
          "inode/directory" = [ "pcmanfm.desktop" ];
        };
      };
    };
  };
}
