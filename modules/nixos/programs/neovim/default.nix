{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.programs.neovim;
in
{
  options.init.programs.neovim = with types; {
    enable = mkEnableOption "neovim";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      neovim

      # Needed by treesitter to compiler his modules
      gcc

      nodePackages.bash-language-server
      nodePackages.typescript-language-server
      # ltex-ls
      nil
      lua-language-server
      pyright
      rPackages.languageserver

      # nodePackages.eslint
      # phpactor
      texlab
      clang-tools_16.out
      jdt-language-server
      (writeShellScriptBin "jdtls" ''
        jdt-language-server "$@"
      '')
    ];

    init.home.extraOptions = {
      xdg.configFile = {
        "nvim" = {
          source = ./config;
          recursive = true;
        };
      };

      xdg.mimeApps = {
        defaultApplications = {
          "application/x-shellscript" = [ "nvim.desktop" ];
          "text/plain" = [ "nvim.desktop" ];
        };
      };
    };
  };
}
