" use true color in the terminal
if (has("termguicolors"))
	set termguicolors
endif

if (has("nvim"))
	let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

set guifont=monospace:h13

syntax enable
colorscheme darkside
