local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable', -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
	-- the colorscheme should be available when starting Neovim
	{
		'usephe/darkside',
		lazy = false, -- make sure we load this during startup if it is your main colorscheme
		priority = 1000, -- make sure to load this before all the other start plugins
	},


	{
		'hrsh7th/nvim-cmp',
		-- load cmp on InsertEnter
		event = 'InsertEnter',
		-- these dependencies will only be loaded when cmp loads
		-- dependencies are always lazy-loaded unless specified otherwise
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
			'L3MON4D3/LuaSnip',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-nvim-lua',
			'saadparwaiz1/cmp_luasnip',
			'hrsh7th/cmp-cmdline',
		},
		config = function()
			require('config.cmp')
		end,
	},

	{
		'neovim/nvim-lspconfig',
		dependencies = {
			-- This plugin adds vscode-like pictograms to neovim built-in lsp
			'onsails/lspkind.nvim',
			'p00f/clangd_extensions.nvim',
			{
				'jose-elias-alvarez/null-ls.nvim',
				dependencies = {'nvim-lua/plenary.nvim'},
			}
		}
	},

	{
		'nvim-treesitter/nvim-treesitter',
		build = ':TSUpdate',
	},

	{
		'nvim-telescope/telescope.nvim',
		dependencies = {'nvim-lua/plenary.nvim'},
		config = function()
			require('config.telescope_setup')
			require('config.telescope')
		end,
	},

	-- emmet-vim provides support for expanding abbreviations similar to emmet
	-- see https://emmet.io/.
	{
		'mattn/emmet-vim',
		ft = {
			'html',
			'css',
			'javascriptreact',
			'javascript',
		},
	},

	{
		'kylechui/nvim-surround',
		opts = {},
	},

	{
		'numToStr/Comment.nvim',
		opts = {},
	},

	'junegunn/vim-easy-align',

	{
		'NvChad/nvim-colorizer.lua',
		-- config = [[require('colorizer').setup {'css', 'javascript', 'vim', 'html', 'yaml'}]],
		config = function()
			require('colorizer').setup({
				filetypes = { '*' },
				user_default_options = {
					RGB = true, -- #RGB hex codes
					RRGGBB = true, -- #RRGGBB hex codes
					names = false, -- "Name" codes like Blue or blue
					RRGGBBAA = false, -- #RRGGBBAA hex codes
					AARRGGBB = false, -- 0xAARRGGBB hex codes
					rgb_fn = false, -- CSS rgb() and rgba() functions
					hsl_fn = false, -- CSS hsl() and hsla() functions
					css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
					css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
					-- Available modes for `mode`: foreground, background,  virtualtext
					mode = 'background', -- Set the display mode.
					-- Available methods are false / true / "normal" / "lsp" / "both"
					-- True is same as normal
					tailwind = false, -- Enable tailwind colors
					-- parsers can contain values used in |user_default_options|
					sass = { enable = false, parsers = { css }, }, -- Enable sass colors
					virtualtext = "■",
				},
				-- all the sub-options of filetypes apply to buftypes
			buftypes = {},
			})
		end,
	},

	-- Testing plugins
	{
		{
			'sheerun/vim-polyglot',
			cond = false,
		},
		{'ctrlpvim/ctrlp.vim'},
		{'nanotech/jellybeans.vim'},
		{'catppuccin/nvim', name = 'catppuccin'},
	},

	{
		'nvim-orgmode/orgmode',
		ft = {'org'},
		config = function()
			require('orgmode').setup_ts_grammar()
			require('orgmode').setup{}
		end
	},
}, {
		dev = {
			-- directory where local plugins are stored
			path = os.getenv('XDG_PROJECTS_DIR') or '~/Projects/personal/',
		},
	})
