-- Set space as the leader key
vim.g.mapleader = ' '

local opt = vim.opt

-- configure the cursor style for each mode.
-- In Normal and Visual mode, use a block cursor.
-- In Insert-likes modes, use a vertical bar cursor.
-- In Replace-likes modes, use a underline cursor.
-- In all modes, blink
opt.guicursor = 'a:blinkwait0-blinkon1000-blinkoff1000,n-v:block,i-c-ci:ver25,r-cr-o:hor25'

-- Don't give the intro message when starting Vim
opt.shortmess:append({ I = true})

-- Enable mouse support in all modes
opt.mouse = 'a'

-- Enable line numbers.
-- Print the numbers relative to the cursor.
-- Except for the current selected line,
-- print the actual line number.
opt.number = true
opt.relativenumber = true

-- Always use the clipboard for all operations
opt.clipboard = "unnamedplus"


-- Disable modelines as a security precaution
opt.modelines = 0
opt.modeline = false

opt.wrap = false

-- Show some invisible characters
opt.listchars = {tab = '» ', space = '·', precedes = '<', extends = '>'}
opt.list = true

opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = 4
opt.expandtab = false

-- Put new window below/right of current one.
opt.splitright = true
opt.splitbelow = true

opt.statusline = '%<%f %((%H%W%R%M)%) %= %y %{&fileencoding?&fileencoding:&encoding} [%{&fileformat}] %-6.((%l,%c%(,%V%))%) %p%%'


-- Make recognizes .h files as c files
vim.g.c_syntax_for_h = true

-- Netrw
vim.g.netrw_banner = 0
