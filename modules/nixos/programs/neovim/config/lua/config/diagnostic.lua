vim.fn.sign_define('DiagnosticSignError', {text = '', numhl = 'DiagnosticSignError'});
vim.fn.sign_define('DiagnosticSignWarn', {text = '', numhl = 'DiagnosticSignWarn'});
vim.fn.sign_define('DiagnosticSignInfo', {text = '', numhl = 'DiagnosticSignInfo'})
vim.fn.sign_define('DiagnosticSignHint', {text = '', numhl = 'DiagnosticSignHint'})
