if exists("b:current_syntax")
  finish
endif
let b:current_syntax = "qf"

" TODO: disable the system's qf syntax entirely
syn match	qfFileName	"^[^:]*" nextgroup=qfSeparator1
syn match	qfSeparator1	":" nextgroup=qfLineNr contained
syn match	qfLineNr	"\d*" nextgroup=qfSeparator2 contained
syn match	qfSeparator2	":" nextgroup=qfColumnNr contained
syn match	qfColumnNr	"\d*" contained

" The default highlighting.
" FIXME: the Title hightlight group doesn't take effect
hi def link qfFileName	Title
hi def link qfLineNr	LineNr
hi def link qfColumnNr	LineNr

