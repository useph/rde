require('config')

require('plugins')

vim.cmd('source $XDG_CONFIG_HOME/nvim/colorscheme.vim')

require('luasnip_config')
