{ lib, config, ... }:
with lib;
let
  cfg = config.init.programs.sxhkd;
in
{
  options.init.programs.sxhkd = with types; {
    enable = mkEnableOption "sxhkd";
  };
  config = mkIf cfg.enable {
    init.home.extraOptions = {
      services.sxhkd = {
        enable = true;
        keybindings = {
          # make sxhkd reload its configuration
          "super + alt + r" = ''pkill -USR1 -x sxhkd && notify-send "sxhkd" "reload the configuration file"'';
          "super + r" = "dmenu_run";
          "super + o; w" = ''notify-send "Openning programe" "Firefox"; $\{BROWSER:-"firefox"\}'';
          "super + shift + Return" = ''$\{TERMINAL:-"/bin/xterm"\}'';

          # Audio Control
          "XF86Audio{Raise,Lower}Volume" = ''pamixer --allow-boost -{i,d} 5 && notify-send -h string:x-canonical-private-synchronous:audio "Volume: " -h int:value:"$(pamixer --get-volume)"'';
          "XF86AudioMute" = "pamixer --toggle-mute && pkill -USR1 -x slstatus";
          "XF86Audio{Prev,Next}" = "playerctl {previous,next}";
          "XF86Audio{Play,Stop}" = "playerctl {play-pause,stop}";
          "super + p; {h,l}" = "playerctl {previous,next}";
          "super + p; {p,m,w}" = "playerctl --player={%any,spotify,firefox} play-pause";
          "super + p; a" = "playerctl -a pause";

          # Change screen brightness
          "XF86MonBrightness{Up,Down}" = "light -{A,U} 5";

          # Take a screenshot
          "Print" = "flameshot full --path  ~/Media/Pictures/screenshots";
          "shift + Print" = "flameshot gui";

          # Utils
          "super + BackSpace" = "sys";
        };
      };
    };
  };
}
