{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.init.programs.tmux;
in
{
  options.init.programs.tmux = with types; {
    enable = mkEnableOption "tmux";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      tmux
    ];

    init.home.extraOptions = {
      programs.tmux = {
        enable = true;
        # Change the prefix key
        prefix = "C-Space";
        # Set the default terminal for new windows to "tmux*".
        # This tells applications inside tmux what the capabilities are for tmux itself.
        terminal = "tmux-256color";
        # Start windows and panes index at 1, instead of 0.
        baseIndex = 1;
        # Enable the mouse support
        mouse = true;
        # Set escape delay
        # https://github.com/neovim/neovim/wiki/FAQ#esc-in-tmux-or-gnu-screen-is-delayed
        escapeTime = 10;
        # Use vi key bindings in copy mode.
        keyMode = "vi";
        # Increase the history size
        historyLimit = 5000;
        plugins = with pkgs.tmuxPlugins; [
          open
          yank
        ];

        extraConfig = builtins.readFile ./tmux.conf;
      };
    };
  };
}
