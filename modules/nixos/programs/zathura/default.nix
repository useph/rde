{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.init.programs.zathura;
in
{
  options.init.programs.zathura = with types; {
    enable = mkEnableOption "zathura";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      zathura
    ];
    init.home.extraOptions = {
      home.file = {
        ".config/zathura/zathura_theme".source = ./zathura_theme;
      };
      programs.zathura = {
        enable = true;
        options = {
          ## use the width auto adjustment mode by default
          adjust-open = "width";

          ## using the clipboard as selection clipboard
          selection-clipboard = "clipboard";

          ## Disable horizontally centered search results
          search-hadjust = "false";

          ## Use basename of the file in the window title
          window-title-basename = "true";

          ## Display the page number in the window title.
          window-title-page = "true";

          ## Display ~ instead of $HOME in the statusbar
          statusbar-home-tilde = "true";

          ## Display (current page / total pages) as a percent in the statusbar
          statusbar-page-percent = "true";

          ## Explicitly set the database as sqlite
          database = "sqlite";
        };

        mappings = {
          "[normal] k" = "scroll half-up";
          "[fullscreen] k" = "scroll half-up";
          "[normal] <S-k>" = "scroll full-up";
          "[fullscreen] <S-k>" = "scroll full-up";
          "[normal] j" = "scroll half-down";
          "[fullscreen] j" = "scroll half-down";
          "[normal] <S-j>" = "scroll full-down";
          "[fullscreen] <S-j>" = "scroll full-down";
          "[normal] <C-e>" = "scroll down";
          "[fullscreen] <C-e>" = "scroll down";
          "[normal] <C-y>" = "scroll up";
          "[fullscreen] <C-y>" = "scroll up";

          "[normal] i" = "recolor";
          "[fullscreen] i" = "recolor";

          "[normal] <C-b>" = "toggle_statusbar";
          "[fullscreen] <C-b>" = "toggle_statusbar";

          "[normal] f" = "adjust_window best-fit";
          "[fullscreen] f" = "adjust_window best-fit";
          "[normal] w" = "adjust_window width";
          "[fullscreen] w" = "adjust_window width";

          "[normal] >" = "rotate rotate-cw";
          "[fullscreen] >" = "rotate rotate-cw";
          "[normal] <" = "rotate rotate-ccw";
          "[fullscreen] <" = "rotate rotate-ccw";

          "[normal] r" = "reload";
          "[fullscreen] r" = "reload";
        };

        extraConfig = ''
          # styling
          ## remove the vertical and horizontal padding of the statusbar and
          ## notification bar
          set statusbar-h-padding 0;
          set statusbar-v-padding 0;

          # the font size is in pt
          # font = "monospace; bold 12"

          ## color scheme
          include zathura_theme

          # test: quick navigation between the two recent files
          map <C-6> feedkeys <S-O><Tab><Return>
          ## show document information
          map <C-g> feedkeys :info<Return>

          unmap [normal]     a
          unmap [fullscreen] a
          unmap [normal]     s
          unmap [fullscreen] s
          unmap [normal]     R
          unmap [fullscreen] R
        '';
      };

      xdg.mimeApps = {
        defaultApplications = {
          "application/pdf" = [ "org.pwmt.zathura.desktop" ];
        };
      };
    };
  };
}
