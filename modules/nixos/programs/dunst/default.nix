{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.init.programs.dunst;
in
{
  options.init.programs.dunst = with types; {
    enable = mkEnableOption "dunst";
  };
  config = mkIf cfg.enable {
    # TODO: replace this dunst service
    environment.systemPackages = with pkgs; [
      dunst
    ];
    init.home.extraOptions = {
      xdg.configFile = {
        "dunst/dunstrc".source = ./dunstrc;
      };
    };
  };
}
