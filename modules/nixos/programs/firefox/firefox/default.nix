{ pkgs, config, lib, inputs, ... }:
with lib;
let
  cfg = config.init.programs.firefox;
in
{
  options.init.programs.firefox = with types; {
    enable = mkEnableOption "firefox";
  };
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      firefox
    ];

    init.home.extraOptions = {
      programs.firefox =
        let
          baseUserJS = builtins.readFile "${inputs.arkenfox-userjs}/user.js";
        in
        {
          enable = true;
          # [See list of policies](https://mozilla.github.io/policy-templates/).
          policies = {
            DisableFirefoxAccounts = false;
            DisableFirefoxStudies = true;
            DisablePocket = true;
            OfferToSaveLogins = false;
            DisableTelemetry = true;
            CaptivePortal = false;
            FirefoxHome = {
              Search = true;
              Pocket = false;
              Snippets = false;
              TopSites = false;
              Highlights = false;
            };
            NoDefaultBookmarks = true;
            PasswordManagerEnabled = false;
            UserMessaging = {
              ExtensionRecommendations = false;
              SkipOnboarding = true;
            };
          };
          profiles = {
            useph = {
              name = "useph";
              isDefault = true;
              id = 0;
              extensions = with pkgs.nur.repos.rycee.firefox-addons; [
                ublock-origin
                sponsorblock
                multi-account-containers
                simplelogin
              ];
              search = {
                force = true;
                default = "Startpage";
                engines =
                  let
                    defaultUpdateInterval = 4 * 24 * 60 * 60 * 1000; # every 4 days
                  in
                  {
                    "SearXNG" = {
                      urls = [{
                        template = "http://priv.au/search";
                        params = [
                          { name = "q"; value = "{searchTerms}"; }
                          { name = "engines"; value = "google,brave,bing,qwant"; }
                        ];
                        method = "post";
                      }];
                      iconUpdateURL = "https://searx.space/favicon.svg";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@sx" "@searx" ];
                    };
                    "Longman" = {
                      urls = [{
                        template = "https://www.ldoceonline.com/search/english/direct/";
                        params = [
                          { name = "q"; value = "{searchTerms}"; }
                        ];
                      }];
                      iconUpdateURL = "https://www.ldoceonline.com/external/images/favicon.ico?version=1.2.66";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@ld" "@longman" ];
                    };
                    "YouTube" = {
                      urls = [{
                        template = "https://www.youtube.com/results";
                        params = [
                          { name = "search_query"; value = "{searchTerms}"; }
                          { name = "page"; value = "{startPage?}"; }
                          { name = "utm_source"; value = "opensearch"; }
                        ];
                      }];
                      iconUpdateURL = "https://www.youtube.com/s/desktop/28b0985e/img/favicon_32x32.png";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@yt" "@youtube" ];
                    };
                    "Yandex" = {
                      urls = [{
                        template = "https://yandex.com/search";
                        params = [
                          { name = "text"; value = "{searchTerms}"; }
                          { name = "from"; value = "os"; }
                        ];
                      }];
                      iconUpdateURL = "https://yastatic.net/s3/home-static/_/92/929b10d17990e806734f68758ec917ec.png";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@yd" "@yandex" ];
                    };
                    "Startpage" = {
                      urls = [{
                        template = "https://www.startpage.com/sp/search";
                        params = [
                          { name = "query"; value = "{searchTerms}"; }
                          { name = "cat"; value = "web"; }
                          { name = "pl"; value = "opensearch"; }
                          { name = "language"; value = "opensearch"; }
                        ];
                      }];
                      iconUpdateURL = "https://www.startpage.com/sp/cdn/favicons/favicon-32x32-gradient.png";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@sp" ];
                    };
                    "Urban Dictionary" = {
                      urls = [{
                        template = "http://www.urbandictionary.com/define.php";
                        params = [
                          { name = "term"; value = "{searchTerms}"; }
                        ];
                      }];
                      iconUpdateURL = "https://www.urbandictionary.com/favicon-32x32.png";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@ud" "@urban" ];
                    };
                    "Jackett" = {
                      urls = [{
                        template = "http://localhost:9117/UI/Dashboard#search={searchTerms}&filter=all";
                      }];
                      iconUpdateURL = "http://localhost:9117/favicon.ico";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@jac" ];
                    };
                    "Brave" = {
                      urls = [{
                        template = "https://search.brave.com/search";
                        params = [
                          { name = "q"; value = "{searchTerms}"; }
                        ];
                      }];
                      iconUpdateURL = "https://cdn.search.brave.com/serp/v2/_app/immutable/assets/brave-search-icon.639e8ddc.svg";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@brave" ];
                    };
                    "Nix Packages" = {
                      urls = [{
                        template = "https://search.nixos.org/packages";
                        params = [
                          { name = "type"; value = "packages"; }
                          { name = "query"; value = "{searchTerms}"; }
                        ];
                      }];
                      icon = "''${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
                      definedAliases = [ "@np" ];
                    };
                    "NixOS Wiki" = {
                      urls = [{ template = "https://nixos.wiki/index.php?search={searchTerms}"; }];
                      iconUpdateURL = "https://nixos.wiki/favicon.png";
                      updateInterval = defaultUpdateInterval;
                      definedAliases = [ "@nw" ];
                    };
                    "Wikipedia (en)".metaData.alias = "@wiki";
                    # http://www.google.com/ncr
                    "Google".metaData.alias = "@g";
                    "Amazon.com".metaData.hidden = true;
                    "Bing".metaData.hidden = true;
                    "eBay".metaData.hidden = true;
                  };
              };
              extraConfig = lib.strings.concatStrings [
                baseUserJS
                ''
                  // user-overrides
                  user_pref("browser.search.region", "US");
                  user_pref("general.smoothScroll", false);
                  user_pref("browser.ctrlTab.sortByRecentlyUsed", true);
                  user_pref("full-screen-api.ignore-widgets", true);
                  user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
                  user_pref("media.ffmpeg.vaapi.enabled", true);
                  user_pref("media.rdd-vpx.enabled", true);
                  user_pref("privacy.sanitize.sanitizeOnShutdown", false);
                  user_pref("browser.tabs.firefox-view", false);
                  user_pref("privacy.resistFingerprinting", false);
                ''
              ];
              userChrome = ''
          '';
              userContent = ''
          '';
            };
            base = {
              name = "base";
              isDefault = false;
              id = 1;
            };
            orion = {
              name = "orion";
              isDefault = false;
              id = 2;
            };
          };
        };

      xdg.mimeApps = {
        associations.added = {
          "x-scheme-handler/http" = [ "firefox.desktop" ];
          "x-scheme-handler/https" = [ "firefox.desktop" ];
          "x-scheme-handler/chrome" = [ "firefox.desktop" ];
        };
        defaultApplications = {
          "x-scheme-handler/http" = [ "firefox.desktop" ];
          "x-scheme-handler/https" = [ "firefox.desktop" ];
          "x-scheme-handler/chrome" = [ "firefox.desktop" ];
        };
      };
    };

  };
}
