{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.init.hardware.networking;
in
{
  options.init.hardware.networking = with types; {
    enable = mkEnableOption "Whether or not to enable networking support";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      networkmanagerapplet
    ];
    init.user.extraGroups = [ "networkmanager" ];

    networking.networkmanager.enable = true;
  };
}
