{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.init.hardware.audio;
in
{
  options.init.hardware.audio = with types; {
    enable = mkEnableOption "Whether or not to enable audio support";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      pulsemixer
    ];

    security.rtkit.enable = true;
    hardware.pulseaudio.enable = false;

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };
  };
}
