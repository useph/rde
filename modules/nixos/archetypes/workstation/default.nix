{ config, lib, ... }:
with lib;
let
  cfg = config.init.archetypes.workstation;
in
{
  options.init.archetypes.workstation = with types; {
    enable = mkEnableOption "Whether or not to enable the workstation archetype.";
  };

  config = mkIf cfg.enable {
    init = {
      suites = {
        common.enable = true;
        desktop.enable = true;
        development.enable = true;
      };
    };
  };
}
