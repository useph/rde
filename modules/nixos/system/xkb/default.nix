{ config, lib, ... }:
with lib;
let
  cfg = config.init.system.xkb;
in
{
  options.init.system.xkb = with types; {
    enable = mkEnableOption "Whether or not to configure xkb.";
  };

  config = mkIf cfg.enable {
    services.xserver.xkb = {
      layout = "us,qwerty-fr";
      extraLayouts = {
        "qwerty-fr" = {
          description = "Qwerty keyboard layout with French accents.";
          languages = [ "eng" "fr" ];
          symbolsFile = ./us_qwerty-fr;
        };
      };
      options = "caps:ctrl_modifier,altwin:menu_win,grp:alts_toggle";
    };
  };
}
