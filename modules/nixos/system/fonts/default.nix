{ config, pkgs, lib, ... }:

with lib;
let
  cfg = config.init.system.fonts;
in
{
  options.init.system.fonts = with types; {
    enable = mkEnableOption "Whether or not to manage fonts.";
  };

  config = mkIf cfg.enable {
    # nixpkgs.config.joypixels.acceptLicense = true;
    # channels-config = {
    #   allowUnfree = true;
    #   joypixels.acceptLicense = true;
    # };
    fonts = {
      packages = with pkgs; [
        ubuntu_font_family
        dejavu_fonts
        fira
        liberation_ttf
        noto-fonts-emoji
        noto-fonts
        joypixels
        (nerdfonts.override { fonts = [ "UbuntuMono" "NerdFontsSymbolsOnly" ]; })
      ];
      fontconfig = {
        enable = true;
        defaultFonts = {
          monospace = [
            "Ubuntu Mono"
            "UbuntuMono Nerd Font"
            "DejaVu Sans Mono"
            "Liberation Mono"
            "Noto Sans Mono"
          ];
          sansSerif = [
            "Ubuntu"
            "DejaVu Sans"
            "Fira Sans"
            "Liberation Sans"
            "Noto Color Emoji"
            "Noto Naskh Arabic"
          ];
          serif = [
            "DejaVu Serif"
            "Noto Color Emoji"
          ];
        };
      };
    };
  };
}
