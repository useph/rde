{ config, lib, ... }:
with lib;
let
  cfg = config.init.system.time;
in
{
  options.init.system.time = with types; {
    enable = mkEnableOption "Whether or not to configure timezone information.";
  };

  config = mkIf cfg.enable {
    time.timeZone = "Africa/Casablanca";
  };
}
