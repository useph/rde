{ config, lib, ... }:
with lib;
let
  cfg = config.init.system.boot;
in
{
  options.init.system.boot = with types; {
    enable = mkEnableOption "Whether or not to enable booting.";
  };

  config = mkIf cfg.enable {
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
  };
}
